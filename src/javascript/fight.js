export function fight(firstFighter, secondFighter) {
  let winner = null;

  while(!winner) {
    secondFighter.health -= getDamage(firstFighter, secondFighter);
    if (secondFighter.health <= 0) {
      winner = firstFighter;
      break;
    }
    firstFighter.health -= getDamage(secondFighter, firstFighter);
    if (firstFighter.health <= 0) {
      winner = secondFighter;
      break;
    }
  }

  return winner;
}

export function getDamage(attacker, enemy) {
  const hit = getHitPower(attacker);
  const block = getBlockPower(enemy);
  return hit >= block 
    ? (hit - block) 
    : 0;
}

export function getHitPower(fighter) {
  return fighter.attack * criticalHitChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * dodgeChance();
}

const random = (min, max) => Math.random() * (max - min) + min;

const criticalHitChance = () => random(1, 2);

const dodgeChance = () => random(1, 2);
