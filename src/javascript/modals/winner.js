import { showModal } from './modal';
import { createElement }  from './../helpers/domHelper';

export function showWinnerModal(fighter) {
  // show winner name and image
  const {name, source: image} = fighter;

  const title = `The winner is: ${name}!!!`;
  const bodyElement = createElement({tagName: 'img', attributes: {src: image}});
  showModal({title, bodyElement});
}