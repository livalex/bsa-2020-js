import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, source: image, attack, defense, health } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const imageElement = createElement({tagName: 'img', className: 'fighter-image'});
  const nameElement = createElement({ tagName: 'h2', className: 'fighter-name' });
  const infoElement = createInfoElement({ attack, defense, health });

  nameElement.textContent = name;
  imageElement.src = image;
  fighterDetails.appendChild(imageElement);
  fighterDetails.append(nameElement);
  fighterDetails.appendChild(infoElement);

  return fighterDetails;
}

function createInfoElement({ attack, defense, health }) {
  const tableAttrs = {
    cellspacing: 0, 
    cellpadding: 5
  };

  const tableElement = createElement({
    tagName: 'table', 
    className: 'modal-table', 
    tableAttrs
  });

  tableElement.appendChild(createRow('Attack', attack));
  tableElement.appendChild(createRow('Defence', defense));
  tableElement.appendChild(createRow('Health', health));

  return tableElement;
}

function createRow (title, value) {
  const row = createElement({tagName: 'tr'});
  const titleEl = createElement({tagName: 'th'});
  titleEl.textContent = title;
  const valueEl = createElement({tagName: 'td'});
  valueEl.textContent = value;
  row.appendChild(titleEl);
  row.appendChild(valueEl);
  return row;
}
